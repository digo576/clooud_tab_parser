class ParserController < ApplicationController

  before_action :verify_file, only: %i(process_file)

  def index; end

  def process_file
    result = TabFileParser.new(file_path: @file_path).call
    if result.success?
      flash[:notice] = I18n.t('views.parser.file_processed')
      redirect_to parser_results_path
    else
      flash[:danger] = result.errors
      redirect_to parser_index_path
    end
  end

  def results
    @clients = Client.all
    @orders = Order.all
    @merchants = Merchant.all
    @items = Item.all
  end

  private

  def verify_file
    @file_path = params[:parser][:input_file].tempfile
    return if @file_path.present?
    flash[:warning] = I18n.t('views.parser.not_sent')
    redirect_to parser_index_path
  end

end
