require 'csv'

class TabFileParser

  def initialize(file_path:)
    @input_file = File.read(file_path)
    @data = CSV.parse(input_file, headers: true, col_sep: "\t", header_converters: :symbol)
  end

  def call
    process
    result
  end

  private

  attr_reader :success, :errors, :result, :data, :input_file

  def errors
    @errors ||= []
  end

  def process
    raise TabFileParserError::EmptyFileError unless data.present?

    data.each do |row|
      purchase = row.to_hash
      ActiveRecord::Base.transaction do
        client = Client.create(name: purchase[:purchaser_name])
        item = Item.find_or_create_by(base_price: purchase[:item_price], description: purchase[:item_description])
        merchant = Merchant.find_or_create_by!(address: purchase[:merchant_address], name: purchase[:merchant_name])
        merchant.items << item unless merchant.items.include? item
        order = Order.new(client: client)
        purchase[:purchase_count].to_i.times do
          order.items << item
        end
        order.save
      end
    end
  rescue StandardError, TabFileParserError::Error => exception
    errors << exception.message
  end

  def result
    OpenStruct.new(success?: errors.try(:flatten).blank?,
                   errors: errors.try(:flatten),
                   data: data)
  end

end