class Item < ApplicationRecord

  belongs_to :merchant

  has_and_belongs_to_many :order

  validates_numericality_of :base_price, greater_than_or_equal_to: 0

  validates_presence_of :base_price
  validates_presence_of :merchant
  validates_presence_of :description

end
