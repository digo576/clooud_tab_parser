class Order < ApplicationRecord

  belongs_to :client

  has_and_belongs_to_many :items

  validates_presence_of :client
  validates_presence_of :items

  def self.all_total_incoming
    Order.all.to_a.sum(&:total)
  end

  def total
    items.sum(:base_price)
  end

end
