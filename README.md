# Clooud tab parser

This is a simple project where it receives a text file (tab separated), proccess it and show the total gross income represented by the sales data.

If you downloaded this project from a .zip, or somewhere else, you can check it online at https://gitlab.com/digo576/clooud_tab_parser for new updates. 

## Project description

You've received a text file (tab separated) with data describing the company sales. We need a way for this data to be imported to a database.

Your job is to create a web interface that accepts file uploads, normalize the data and store it in a relational database.

Your application MUST:

1. Accept (via HTML form) file uploads of TAB-separated files, with the following columns: `purchaser name`, `item description`, `item price`, `purchase count`, `merchant address`, `merchant name`. You can assume the columns will always be in that order, and that there will always be some value in each column, and that there will always be a header row. An example file called `example_input.tab` is included on this repo.
2. Interpret (parse) the received file, normalize the data, and save the data correctly in a relational database.
3. Show the total gross income represented by the sales data after file upload.
4. Be written in Ruby 2.4 or greater (or, in the language solicited by the job description, if any).
5. Be simple to configure and execute, running on a Unix-compatible environment (Linux or macOS).
6. Use only free / open-source language and libraries.

Your application doesn't need to:

1. Deal with authentication or authorization (bonus points if it does, though, specially via oAuth).
2. Be developed with any specific framework (but there's nothing wrong with using them, use what you think is best for the task).
3. Be pretty.

## Characteristics

| Technology | Version |
|:----------:|:------:|
|    Ruby    |  2.5.1p57 |
|    Rails   |   5.2.2  |
|     RVM    | 1.29.4 |

## Dependencies installation

To run this application, you are going to need a Ubuntu Linux environment wit the following dependencies:

- PostgreSQL;
- RVM - Ruby Version Manager;
- Ruby;

### PostgreSQL installation

After installing and setting up PostgreSQL (if you need you can follow [these instruction](https://www.digitalocean.com/community/tutorials/como-instalar-e-utilizar-o-postgresql-no-ubuntu-16-04-pt) to install it).

Next, run the following command at the command line to install some PostgreSQL dependencies:

```SHELL
sudo apt-get install -y libpq-dev
``` 

### RVM - Ruby Version Manager

You can follow [these instruction](https://www.digitalocean.com/community/tutorials/how-to-install-ruby-on-rails-with-rvm-on-ubuntu-16-04), or run the following commands:

```SHELL
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
cd /tmp
curl -sSL https://get.rvm.io -o rvm.sh
cat /tmp/rvm.sh | bash -s stable --rails
```

### Ruby

Last, but not least, you should install `ruby 2.5.1`. Just run the following command:

```SHELL
rvm install ruby-2.5.1
```

## Project setup
> If you already downloaded this project, go to the second step.

1. Clone this project using your favorite git client or command line with the following command:

```shell
$ git clone git@gitlab.com:digo576/clooud_tab_parser.git
```

2. Go to the project folder and run the following command to install its dependencies:

```shell
$ bundle
```

3. run the following commands to setup its database:

```shell
$ rails db:setup && rails db:migrate
```

## Running it

To run the project, you just need to run the following command at the project's root folder:

```
$ rails s
```

Wait for it to start and open your favorite browser. Now you can access it at `localhost:3000` URL.

## Running tests

If you would like to check units tests, just run the following command at the project's root folder:

```shell
$ rspec 
```

___
- Developed by Rodrigo Marques.
- rodrigomarques1993@gmail.com
- [LinkedIn](https://www.linkedin.com/in/rodrigopedromarques/)
- [GitHub](https://github.com/rodrigo93)