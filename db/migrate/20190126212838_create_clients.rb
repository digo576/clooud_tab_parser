class CreateClients < ActiveRecord::Migration[5.2]
  def self.up
    create_table :clients do |t|
      t.column :name, :string

      t.timestamps
    end
  end

  def self.down
    drop_table :clients
  end
end