class CreateItems < ActiveRecord::Migration[5.2]
  def self.up
    create_table :items do |t|
      t.column :description, :string
      t.column :base_price, :decimal, precision: 8, scale: 2
      t.references :merchant, foreign_key: true, index: true

      t.timestamps
    end
  end

  def self.down
    drop_table :items
  end
end
