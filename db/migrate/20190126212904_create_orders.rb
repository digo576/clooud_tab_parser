class CreateOrders < ActiveRecord::Migration[5.2]
  def self.up
    create_table :orders do |t|
      t.references :client, foreign_key: true, index: true
      t.references :item, foreign_key: true, index: true

      t.timestamps
    end
  end

  def self.down
    drop_table :orders
  end
end
