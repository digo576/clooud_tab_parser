class CreateMerchants < ActiveRecord::Migration[5.2]
  def self.up
    create_table :merchants do |t|
      t.column :address, :string
      t.column :name, :string

      t.timestamps
    end
  end

  def self.down
    drop_table :merchants
  end
end
