Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'parser#index'

  namespace :parser do
    get 'index'
    post 'process_file'
    get 'results'
  end

end
