FactoryBot.define do
  factory :client do
    name { Faker::RickAndMorty.character }
  end
end
