FactoryBot.define do
  factory :order do

    # Fks
    client

    after :build, :stub do |order|
      order.items << build(:item)
    end


    trait :with_some_items do
      transient do
        item_quantity { rand (5) + 1 }
      end

      after :build do |order, evaluator|
        item_list = create_list :item, evaluator.item_quantity
        order.items << item_list
      end
    end

  end
end
