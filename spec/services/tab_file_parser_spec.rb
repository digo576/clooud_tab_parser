require 'rails_helper'

RSpec.describe TabFileParser, type: :service do

  describe '.call' do
    context 'when passing valid parameters' do
      let(:service) { TabFileParser.new(file_path: 'spec/files/sample_input.tab') }
      let(:result) { service.call }

      it 'should be successfull' do
        expect(result.success?).to be_truthy
      end

      it 'should create Clients' do
        expect { result }.to change { Client.count }.by(4)
      end

      it 'should create Merchants' do
        expect { result }.to change { Merchant.count }.by(3)
      end

      it 'should create Items' do
        expect { result }.to change { Item.count }.by(3)
      end

      it 'should create Orders' do
        expect { result }.to change { Order.count }.by(4)
      end
    end

    context 'when passing an empty file' do
      let(:service) { TabFileParser.new(file_path: 'spec/files/empty_input.tab') }
      let(:result) { service.call }

      it 'should return success as false' do
        expect(result.success?).to be_falsey
      end

      it "should contain the error message #{I18n.t('tab_file_parser_error.empty_file')}" do
        expect(result.errors).to include(I18n.t('tab_file_parser_error.empty_file'))
      end
    end

  end

end
