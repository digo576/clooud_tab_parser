require 'rails_helper'

RSpec.describe Item, type: :model do

  it { is_expected.to belong_to :merchant }

  it { is_expected.to have_and_belong_to_many :order }

  it { is_expected.to validate_numericality_of(:base_price).is_greater_than_or_equal_to(0) }

  it { is_expected.to validate_presence_of :base_price }
  it { is_expected.to validate_presence_of :merchant }
  it { is_expected.to validate_presence_of :description }

  describe 'Factory' do
    context 'when creating the base factory' do
      it 'should be created without any errors' do
        expect(create(:item)).to be_truthy
      end
    end
  end

end
