require 'rails_helper'

RSpec.describe Order, type: :model do

  it { is_expected.to belong_to :client }

  it { is_expected.to have_and_belong_to_many :items }

  it { is_expected.to validate_presence_of :client }
  it { is_expected.to validate_presence_of :items }

  describe 'Factory' do
    context 'when creating the base factory' do
      it 'should be created without any errors' do
        expect(create(:order)).to be_truthy
      end
    end

    context 'when created with trait :with_some_items' do
      let(:order) { create(:order, :with_some_items) }
      
      it 'should be created without any errors' do
        expect(create(:order, :with_some_items)).to be_truthy
      end

      it 'should have at least 1 item' do
        expect(order).to have_at_least(1).items
      end
    end
  end

  describe '.all_total_incoming' do
    context 'when there is no Orders' do
      it 'should return 0' do
        expect(Order.all_total_incoming).to be_zero
      end
    end

    context 'when there are Orders' do
      before :each do
        number_of_orders = rand (10) + 1
        create_list :order, number_of_orders, :with_some_items
      end

      it 'should return the sum of all Orders total' do
        total_income = 0
        Order.all.each do |order|
          total_income += order.total
        end
        expect(Order.all_total_incoming).to eq(total_income)
      end
    end
  end

  describe '#total' do
    let(:order) { create(:order, :with_some_items) }

    it 'should return the sum of all its items' do
      order_total = order.items.sum(:base_price)
      expect(order.total).to eq(order_total)
    end
  end

end
