require 'rails_helper'

RSpec.describe Merchant, type: :model do

  it { is_expected.to have_many :items }

  it { is_expected.to validate_presence_of :name }

  describe 'Factory' do
    context 'when creating the base factory' do
      it 'should be created without any errors' do
        expect(create(:merchant)).to be_truthy
      end
    end
  end

end
