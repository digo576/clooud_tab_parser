module TabFileParserError

  class Error < StandardError
    def initialize(message)
      super(message)
    end
  end

  class EmptyFileError < Error
    def initialize
      super(I18n.t('tab_file_parser_error.empty_file'));
    end
  end

end
